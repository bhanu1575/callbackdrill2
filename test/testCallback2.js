/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
        Then pass control back to the code that called it by using a callback function.
*/
const lists = require('../lists_1.json');
const getLists = require('../callback2');

const id = 'mcu453ed';

function callback(data){
 console.log(data);
}
try {
    (getLists(lists,id,callback));
} catch (error) {
    console.error(error.message);
}
