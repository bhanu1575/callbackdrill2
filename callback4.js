/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const getBoardInformation = require("./callback1");
const getLists = require("./callback2");
const getCards = require("./callback3");




function getDetails(boards,lists,cards){
    setTimeout(()=>{
        getBoardInformation(boards,'mcu453ed',(data)=>{
            console.log('Board Information:',data);
            getLists(lists,'mcu453ed',(data)=>{
                console.log('Lists:',data);
                let cardId = data.find((list)=>list.name == 'Mind').id;
                getCards(cards,cardId,(data)=>{
                    console.log('cards:',data);
                })
            })
        })
    },2000)
}


module.exports = getDetails;