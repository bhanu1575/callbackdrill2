/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

function getBoardInformation(boards,id,callback){
    setTimeout(()=>{
        let results = boards.filter((board) => board.id === id);
        callback(results)
    },2000)
}


module.exports = getBoardInformation;