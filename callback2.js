/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
        Then pass control back to the code that called it by using a callback function.
*/
function getLists(lists,id,callback){
    setTimeout(()=>{
        let results = lists[id] ? lists[id] : [];
        callback(results);
    },2000)
}

module.exports = getLists