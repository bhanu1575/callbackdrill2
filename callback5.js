/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const getBoardInformation = require("./callback1");
const getLists = require("./callback2");
const getCards = require("./callback3");




function getDetails(boards,lists,cards){
    setTimeout(()=>{
        getBoardInformation(boards,'mcu453ed',(data)=>{
            console.log('Board Information:',data);
            getLists(lists,'mcu453ed',(data)=>{
                console.log('Lists:',data);
                let filtered_cards = data.filter((list)=>list.name == 'Mind' || list.name == 'Space');
                filtered_cards.forEach((card)=>{
                    getCards(cards,card.id,(data)=>{
                        console.log('cards:',data);
                    })
                })
            })
        })
    },2000)
}


module.exports = getDetails;