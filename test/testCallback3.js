/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/
const cards = require('../cards.json');
const getCards = require('../callback3');

let id = 'qwsa221';

function callback(data){
    console.log(data);
}
try {
    getCards(cards,id,callback)
} catch (error) {
    console.log(error);
}